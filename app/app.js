var React = require('react');
var Router = require('react-router');
var {Route, DefaultRoute, NotFoundRoute, RouteHandler} = Router;

var Header = require('./components/Header');
var Footer = require('./components/Footer');

var {Link} = require('react-router');

var App = React.createClass({
  render: function () {
    return (
      <div className="wrapper">
           <Header />

           <div className="main-content">
              <RouteHandler/>
           </div>

      </div>
    );
  }
});

var PageUsersHome = require('react-proxy-plus?name=page-users-home!./components/Pages/User/Home');
var UserSinglePage = require('react-proxy-plus?name=page-users-single!./components/Pages/User/Single');

var PageTimesheetsHome = require('react-proxy-plus?name=page-timesheets-home!./components/Pages/Timesheet/Home');
var TimesheetSinglePage = require('react-proxy-plus?name=page-timesheets-single!./components/Pages/Timesheet/Single');

var routes = (
  <Route name="app" path='/' handler={App}>
    <DefaultRoute handler={PageTimesheetsHome}/>

    <Route name="page-user-home" path="user" handler={PageUsersHome}/>
    <Route name="page-single-user" path="user/:id" handler={UserSinglePage}/>

    <Route name="page-timesheets-home" path="timesheets" handler={PageTimesheetsHome}/>
    <Route name="page-single-timesheet" path="timesheet/:id" handler={TimesheetSinglePage}/>
  </Route>
);

function run() {
  Router.run(routes, Router.HistoryLocation, function (Handler) {
    if (document.body.className.indexOf('render') === -1) {
      document.body.className += document.body.className.length ? ' render' : 'render';
    }
    React.render(<Handler/>, document.body);
  });
}

if (window.addEventListener) {
  window.addEventListener('DOMContentLoaded', run);
} else {
  window.attachEvent('onload', run);
}
