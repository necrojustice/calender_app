module.exports = {

  getData: function(itemId, compareId, data) {
    var items = [];

    for (var ticketId in data) {
      if(data[ticketId][compareId] == itemId && !data[ticketId].parent){
        items.push(data[ticketId]);
      }
     } 

    return items;
  },

  getSingleData: function(id, data) {
    for (var ticketId in data) {
	    if(data[ticketId].id == id){
	    	return data[ticketId]
	    }
	   }    

     return null;
  },

  getParentData: function(id, data) {
    var parentTicket = null;

    for (var ticketId in data) {
      if(data[ticketId].id == id){
        if(data[ticketId].parent){
          for(var parentId in data){
            if(data[parentId].id == data[ticketId].parent){
              parentTicket = data[parentId];
            }
          }
        }
      }
     }

    return parentTicket;    
  },

  getChildrenData: function(id, data){
    var tickets = [];

    for (var ticketId in data) {
      if(data[ticketId].parent == id){
        tickets.push(data[ticketId]);
      }
     } 

     return tickets;
  }

};