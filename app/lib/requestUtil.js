var request = require('superagent');

// Todo: Replace testing variable and testUrl with constants: JG
var testing = false;
var testUrl = 'http://localhost:1337';

module.exports = {
  combinePath: function(path){
    return testUrl + "/" + path;
  },

  all: function(path, params) {
    var req = request
                .get(this.combinePath(path));

    if(params){
      for (var i = 0; i < params.length; i++) {
        var obj = {};
        var fieldName = params[i]['fieldName'];
        var fieldValue = params[i]['fieldValue'];
        
        obj[fieldName] = fieldValue;

        req.query(obj);
      }
    }
    
    return new Promise(function (resolve, reject){
       req.end(function(err, res){
            resolve(res.body);
          });
    });
  },

  create: function(path, saveParams) {
    var req = request
                .post(this.combinePath(path));

    if(saveParams){
      for (var i = 0; i < saveParams.length; i++) {
        var obj = {};
        var fieldName = saveParams[i]['fieldName'];
        var fieldValue = saveParams[i]['fieldValue'];
        
        obj[fieldName] = fieldValue;

        req.send(obj);
      }
    }
    
    return new Promise(function (resolve, reject){
       req.end(function(err, res){
            resolve(res.body);
          });
    });
  },

  save: function(path, saveParams) {
    var req = request
                .put(this.combinePath(path));

    if(saveParams){
      for (var i = 0; i < saveParams.length; i++) {
        var obj = {};
        var fieldName = saveParams[i]['fieldName'];
        var fieldValue = saveParams[i]['fieldValue'];
        
        obj[fieldName] = fieldValue;

        req.send(obj);
      }
    }
    
    return new Promise(function (resolve, reject){
       req.end(function(err, res){
            resolve(res.body);
          });
    });
  },

  remove: function(path) {
    var req = request
                .del(this.combinePath(path));
    
    return new Promise(function (resolve, reject){
       req.end(function(err, res){
            resolve(res.body);
          });
    });
  },
};