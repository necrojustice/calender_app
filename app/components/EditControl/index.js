var React = require('react');

var EditDataField = require('../EditDataField');
var EditDataControls = require('../EditDataControls');

function getEditControlState() {
  return {
    editableText: null,
    isEditingText: false
  };
}

var EditControl = React.createClass({
  getInitialState: function() {
    return getEditControlState();
  },

  componentDidMount: function() {
    this.setState({editableText: this.props.editableText});
  },

  render: function () {
    return (
        <div>
          <EditDataField 
            fieldId={this.props.fieldId} 
            isEditingField={this.state.isEditingText} 
            fieldContent={this.props.fieldContent} 
            onChangeFunction={this.handleTextChange}
            hideDisplay={this.props.hideDisplay} />

          <EditDataControls
              actionText={this.props.actionText}
              isEditingField={this.state.isEditingText} 
              onToggleClick={this._toggleEdit} 
              onCancelClick={this._cancelEdit} />
        </div>
    );
  },

  _onChange: function() {
    this.setState(getEditControlState());
  },

  handleTextChange: function(e) {
    var obj = {};
    obj.editableText = e.target.value;
    this.setState(obj);
  },

  _toggleEdit: function(){
    this.setState({isEditingText: !this.state.isEditingText});

    switch(this.state.isEditingText){
      case true:
        var obj = {};
        obj.nameField = this.props.nameField;
        obj.value = this.state.editableText;

        this.props.handleSave(obj);
      break;
    }
  },

  _cancelEdit: function(){
    this.setState({isEditingText: false});
  },
});

module.exports = EditControl;
