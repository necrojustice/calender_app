var React = require('react');
var EditableDiv = require('react-wysiwyg-editor');

var EditDataField = React.createClass({

   _renderEditField: function(){
     if(this.props.isEditingField){
      var editorStyle = {
        overflow: 'auto',
        height: 100,
        maxHeight: 100
      }

      return <EditableDiv style={editorStyle} content={this.props.fieldContent} onChange={this.props.onChangeFunction} />;
    }
    
    if(!this.props.hideDisplay){
      return <p id={this.props.fieldId}>{this.props.fieldContent}</p>;
    }
  },

  render: function () {
    return (
         <div>
          {this._renderEditField()}
         </div>
    );
  }
});

module.exports = EditDataField;
