var React = require('react');

var EditDataControls = React.createClass({

   _renderEditControls: function(){
    if(this.props.isEditingField){
      return <div>
              <button onClick={this.props.onToggleClick} className="btn">
                <span className="fa fa-save"></span> Save
              </button>

              <button onClick={this.props.onCancelClick} className="btn">
                <span className="fa fa-cancel"></span> Cancel
              </button>
             </div>;
    }
    
    return <div>
              <button onClick={this.props.onToggleClick} className="btn">
                <span className="fa fa-pencil"></span> {this.props.actionText ? this.props.actionText : 'Edit'}
              </button>
            </div>;
  },

  render: function () {
    return (
         <div>
          {this._renderEditControls()}
         </div>
    );
  }
});

module.exports = EditDataControls;
