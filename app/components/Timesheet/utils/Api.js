var requestUtil = require('../../../lib/requestUtil');

var Actions = require('../actions/Actions');

var AuthStore = require('./../../Auth/stores/Store');

// Todo: Replace path variable with constant: JG
var path = 'timesheet';

module.exports = {

  getData: function() {
     var user = AuthStore.getUser();

    if(user.role == 'Admin'){
      requestUtil.all(path)
        .then(function(result){
          Actions.load(result);
        });
    }else{
      requestUtil.all(path, [
          {'fieldName':'created_by', 'fieldValue': user.id}
        ])
        .then(function(result){
          Actions.load(result);
        });
    }
  },

  getSingleData: function(id) {
      requestUtil.all(path, [
                              {'fieldName':'id', 'fieldValue': id}
                            ])
      .then(function(result){
        Actions.loadSingle(result);
      });
  },

  create: function(item) {
    requestUtil.create(path ,
                              [
                                {'fieldName':'description', 'fieldValue': item.description},
                                {'fieldName':'hours', 'fieldValue': item.hours}
                              ])
    .then(function(result){
      Actions.create(result);
    });
  },

  remove: function(item) {
    requestUtil.remove(path + "/" + item.id)
    .then(function(result){
      Actions.remove(result);
    });
  },

  save: function(item) {
    requestUtil.save(path + "/" + item.id,
                                 [
                                    {'fieldName':'description', 'fieldValue': item.description},
                                    {'fieldName':'hours', 'fieldValue': item.hours}
                                  ])
    .then(function(result){
      Actions.save(result);
    });
  }

};
