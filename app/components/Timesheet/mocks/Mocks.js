module.exports = {
  init: function() {
    localStorage.removeItem('mockProjects');
    localStorage.setItem('mockProjects', JSON.stringify([
      {
        "id": 1,
        "name": "Game Gamer Website",
        "createdAt": 1436529600,
        "modifiedAt": 1436531400,
        "createdBy": 1
      },
      {
        "id": 2,
        "name": "Buildings R Us Redesign",
        "createdAt": 1436529600,
        "modifiedAt": 1436531400,
        "createdBy": 2
      }
    ]));
  }

};