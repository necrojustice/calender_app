var React = require('react');
var {Link} = require('react-router');
var ItemActions = require('./actions/Actions');

var Item = React.createClass({
  render: function () {
    return (
        <div>
           <Link to={"page-single-timesheet"} params={{id:this.props.id}}>
            <div className="col-md-3">
                  <div className="panel widget">
                     <div className="panel-body">
                        <div className="text-right text-muted">
                           <em className="fa fa-users fa-2x"></em>
                        </div>

                        <h3 className="mt0">Description: {this.props.description}</h3>
                        <p className="text-muted">Hours: {this.props.hours}</p>
                        <div className="progress progress-striped progress-xs">
                        </div>
                     </div>
                  </div>
               </div>
            </Link>
        </div>
    );
  },

   _onDestroyClick: function() {
     ItemActions.destroy(this.props.id);
  }
});

module.exports = Item;
