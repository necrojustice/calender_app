import React from 'react';
import Item from './Item';

class Section extends React.Component{
  _renderItems() {
      var items = null;

      if (this.props.items.length > 0) {
        var items = this.props.items.map(function(item){
          return <Item id={item.id} description={item.description} hours={item.hours} />;
        });
      }

      return items;
    }

    render(){
    if(!this.props.items || this.props.items.length ===0){
      return <div>No Entries.</div>;
    }

    return (
        <div className="row">
            {this._renderItems()}
        </div>
    );
  }
};

module.exports = Section;
