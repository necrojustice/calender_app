var React = require('react');

var RemoveDataControls = React.createClass({
  render: function () {
    return (
      <button onClick={this.props.onButtonClick.bind(null, this.props.id)} className="btn btn-danger">
        <span className="fa fa-trash"></span> Delete
      </button>
    );
  }
});

module.exports = RemoveDataControls;
