var React = require('react');
var Document = require('../../Document');
var Section = require('../../Timesheet/Section');
var Store = require('../../Timesheet/stores/Store');
var Api = require('../../Timesheet/utils/Api');

var EditControl = require('../../EditControl');

function getState() {
  return {
    allItems: Store.getAll()
  };
}

var Home = React.createClass({
  getInitialState: function() {
    return getState();
  },

  componentDidMount: function() {
    Store.addChangeListener(this._onChange);
    Api.getData();
  },

  componentWillReceiveProps: function() {
    Api.getData();
  },

  componentWillUnmount: function() {
    Store.removeChangeListener(this._onChange);
  },

  render: function () {
    return (
      <Document title="PageNested | React-Flux" bodyclassName="page-nested-default">

        <div className="row">
          <h3>Timesheets</h3>

        	<Section items={this.state.allItems} />
        </div>
      </Document>
    );
  },

   /**
   * Event handler for 'change' events coming from the TodoStore
   */
  _onChange: function() {
    this.setState(getState());
  },
});

module.exports = Home;
