var React = require('react');
var Document = require('../../Document');
var {Link} = require('react-router');

var Section = require('../../Timesheet/Section');
var Store = require('../../Timesheet/stores/Store');
var Api = require('../../Timesheet/utils/Api');

var RemoveDataControls = require('../../RemoveDataControls');

var EditControl = require('../../EditControl');

function getState() {
  return {
    item: Store.getSingle()
  };
}

var PageSingleTimesheet = React.createClass({
  getInitialState: function() {
    return getState();
  },

  componentDidMount: function() {
    Store.addChangeListener(this._onChange);

    this.firePageLoad(this.props.params.id);
  },

  componentWillUnmount: function() {
    Store.removeChangeListener(this._onChange);
  },

  firePageLoad: function (itemId) {
    Api.getSingleData(itemId);
  },

  render: function () {
    if(! this.state.item){
      return <div></div>;
    }

    return (
      <Document title="PageNested | React-Flux" bodyclassName="page-nested-default">
        <div>
          <Link to={"page-timesheets-home"}>
            <h3 className="mt0">Timesheets</h3>
          </Link>

          <div className="panel panel-default">
            <div className="panel-heading">Description</div>
            <div className="panel-body">
              <EditControl
                fieldId={'description'}
                nameField={'description'}
                actionText={'Edit'}
                fieldContent={this.state.item.description}
                handleSave={this._save} />
            </div>
          </div>


          <section>
            <div className="row">

               <div className="col-md-3">
                <div className="panel widget">
                   <div className="panel-body">
                      <div className="text-right text-muted">
                        <em className="fa fa-users fa-2x"></em>
                      </div>

                        <span>Hours: </span>
                       <EditControl
                         fieldId={'hours'}
                         nameField={'hours'}
                         actionText={'Edit'}
                         fieldContent={this.state.item.hours}
                         handleSave={this._save} />

                   </div>
                  </div>
               </div>
            </div>


          </section>

          <RemoveDataControls
            onButtonClick={this._deleteItem}
            id={this.state.item.id} />
        </div>
      </Document>
    );
  },

   /**
   * Event handler for 'change' events coming from the TodoStore
   */
  _onChange: function() {
    this.setState(getState());
  },

  _deleteItem: function(id){
    Api.remove(this.state.item);
  },

  _save: function(data){
    var obj = this.state.item;
    obj[data.nameField] = data.value;

    this.setState(obj);
    Api.save(this.state.item);
  },
});

module.exports = PageSingleTimesheet;
