var React = require('react');
var Document = require('../../Document');
var {Link} = require('react-router');

var Section = require('../../User/Section');
var Store = require('../../User/stores/Store');
var Api = require('../../User/utils/Api');

var RemoveDataControls = require('../../RemoveDataControls');

var EditControl = require('../../EditControl');

function getState() {
  return {
    item: Store.getSingle()
  };
}

var PageSingleUser = React.createClass({
  getInitialState: function() {
    return getState();
  },

  componentDidMount: function() {
    Store.addChangeListener(this._onChange);

    this.firePageLoad(this.props.params.id);
  },

  componentWillUnmount: function() {
    Store.removeChangeListener(this._onChange);
  },

  firePageLoad: function (itemId) {
    Api.getSingleData(itemId);
  },

  render: function () {
    if(! this.state.item){
      return <div></div>;
    }

    return (
      <Document title="PageNested | React-Flux" bodyclassName="page-nested-default">
        <div>
          <Link to={"page-user-home"}>
            <h3 className="mt0">Users</h3>
          </Link>

          <div className="panel panel-default">
            <div className="panel-heading">User</div>
            <div className="panel-body">
              <EditControl
                fieldId={'firstName'}
                nameField={'firstName'}
                actionText={'Edit'}
                fieldContent={this.state.item.firstName}
                handleSave={this._save} />

              <EditControl
                fieldId={'lastName'}
                nameField={'lastName'}
                actionText={'Edit'}
                fieldContent={this.state.item.lastName}
                handleSave={this._save} />

              <EditControl
                fieldId={'email'}
                nameField={'email'}
                actionText={'Edit'}
                fieldContent={this.state.item.email}
                handleSave={this._save} />
            </div>
          </div>

          <RemoveDataControls
            onButtonClick={this._deleteItem}
            id={this.state.item.id} />
        </div>
      </Document>
    );
  },

   /**
   * Event handler for 'change' events coming from the TodoStore
   */
  _onChange: function() {
    this.setState(getState());
  },

  _deleteItem: function(id){
    Api.remove(this.state.item);
  },

  _save: function(data){
    var obj = this.state.item;
    obj[data.nameField] = data.value;

    this.setState(obj);
    Api.save(this.state.item);
  },
});

module.exports = PageSingleUser;
