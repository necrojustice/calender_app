var React = require('react');
var {Link} = require('react-router');
var AuthStore = require('./Auth/stores/Store');

function getState() {
  return {
    currentUser: AuthStore.getUser()
  };
}

var Header = React.createClass({
  getInitialState: function() {
    return getState();
  },

  /*
  ** Of course in an actual app I would never store user information in the client nor have it bootstrap in such a hackneyed way - JG
  */
  componentDidMount: function(){
    AuthStore.addChangeListener(this._onChange);

    if(!this.state.currentUser){
      this.loginAsStandard();
    }
  },

  componentWillUnmount: function(){
    AuthStore.removeChangeListener(this._onChange);
  },

  _renderUser: function(){
    if(!this.state.currentUser) return <div></div>;

    return <div>Logged in as: ({this.state.currentUser.role}) {this.state.currentUser.firstName} {this.state.currentUser.lastName}</div>;
  },

  loginAsStandard: function(){
    var user = {"id": 1,
                "firstName": "Lola",
                "lastName": "Delacruz",
                "role": "User",
                "email": "delacruzcampos@zaphire.com"};

    AuthStore.setUser(user);

    window.location.reload();
  },

  loginAsAdmin: function(){
    var user = {"id": 8,
                "firstName": "Felecia",
                "lastName": "Darlene",
                "role": "Admin",
                "email": "darlenecampos@zaphire.com"};

    AuthStore.setUser(user);

    window.location.reload();
  },

  /**
   * Event handler for 'change' events coming from the TodoStore
   */
  _onChange: function() {
    this.setState(getState());
  },

  render: function () {
    if(!this.state.currentUser) return null;

    var userList = this.state.currentUser.role == 'Admin' ?
      <li> <Link to={"page-user-home"}>Users</Link></li> : '';
    
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul className="nav navbar-nav">
              {userList}
              <li><Link to={"page-timesheets-home"}>Timesheets</Link></li>
              <li><a onClick={this.loginAsStandard} href="#">Log in as Standard User</a></li>
              <li><a onClick={this.loginAsAdmin} href="#">Log in as Admin User</a></li>
            </ul>
          </div>
        </div>

        {this._renderUser()}
      </nav>


    );
  }
});

module.exports = Header;
