/*
 * Copyright (c) 2014-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * Actions
 */

var AppDispatcher = require('../../../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');

var Actions = {
  remove: function(data) {
    AppDispatcher.dispatch({
      actionType: Constants.REMOVE,
      data: data
    })
  },

  create: function(data) {
    AppDispatcher.dispatch({
      actionType: Constants.CREATE,
      data: data
    })
  },

  save: function(data) {
    AppDispatcher.dispatch({
      actionType: Constants.SAVE,
      data: data
    })
  },

   load: function(data) {
    AppDispatcher.dispatch({
      actionType: Constants.LOAD,
      data: data
    });
  },

   loadSingle: function(data) {
    AppDispatcher.dispatch({
      actionType: Constants.LOAD_SINGLE,
      data: data
    });
  },

};

module.exports = Actions;
