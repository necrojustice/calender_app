var requestUtil = require('../../../lib/requestUtil');

var Actions = require('../actions/Actions');

// Todo: Replace path variable with constant: JG
var path = 'user';

module.exports = {

  getData: function() {
     requestUtil.all(path)
      .then(function(result){
        Actions.load(result);
      });
  },

  getSingleData: function(id) {
      requestUtil.all(path, [
                              {'fieldName':'id', 'fieldValue': id}
                            ])
      .then(function(result){
        Actions.loadSingle(result);
      });
  },

  create: function(item) {
    requestUtil.create(path ,
                              [
                                {'fieldName':'firstName', 'fieldValue': item.firstName},
                                {'fieldName':'lastName', 'fieldValue': item.lastName},
                                {'fieldName':'email', 'fieldValue': item.email}
                              ])
    .then(function(result){
      Actions.create(result);
    });
  },

  remove: function(item) {
    requestUtil.remove(path + "/" + item.id)
    .then(function(result){
      Actions.remove(result);
    });
  },

  save: function(item) {
    requestUtil.save(path + "/" + item.id,
                                 [
                                   {'fieldName':'firstName', 'fieldValue': item.firstName},
                                   {'fieldName':'lastName', 'fieldValue': item.lastName},
                                   {'fieldName':'email', 'fieldValue': item.email}
                                  ])
    .then(function(result){
      Actions.save(result);
    });
  }

};
