/*
 * Copyright (c) 2014, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * Store
 */

var AppDispatcher = require('../../../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/Constants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _s = [];
var _;

function loadData(data) {
  _s = data;
}

function loadSingleData(data) {
  _ = data;
}

var Store = assign({}, EventEmitter.prototype, {

  /**
   * Get the entire collection of s.
   * @return {object}
   */
  getAll: function() {
    return _s;
  },

  getSingle: function() {
    return _;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register callback to handle all updates
AppDispatcher.register(function(action) {
  switch(action.actionType) {
    case Constants.LOAD:
      loadData(action.data);
      Store.emitChange();
      break;

    case Constants.LOAD_SINGLE:
      loadSingleData(action.data);
      Store.emitChange();
      break;

    case Constants.CREATE:
        _s.push(action.data);
        Store.emitChange();
      break;

    case Constants.REMOVE:
        Store.emitChange();
      break;

    default:
      // no op
  }
});

module.exports = Store;
