/*
 * Copyright (c) 2014-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * Constants
 */

var keyMirror = require('keymirror');

module.exports = keyMirror({
  CREATE: null,
  REMOVE: null,
  SAVE: null,
  LOAD: null,
  LOAD_SINGLE: null
});
